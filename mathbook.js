const _ = require('lodash')
const algebrite = require('algebrite')
const nj = require('numjs')
const plt = require('plotly-notebook-js')

const sOut = []
const evOut = []

const latexHtml = (latex) =>`
<!DOCTYPE html>
<html>
<head>
<title>MathJax TeX Test Page</title>
<script type="text/x-mathjax-config">
  MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$'], ['\\(','\\)']]}});
</script>
<script type="text/javascript" async
  src="https://example.com/mathjax/MathJax.js?config=TeX-AMS_CHTML">
</script>
</head>
<body>
$$${latex}$$
</body>
</html>
`

/**
 * evaluate - a recursive helper function for ev
 */
const evaluate = (domains, expression, subDomain = {}, output = {}) => {
    const key = _.keys(domains)[0]
    const values = domains[key].tolist()
    _.forEach(values, (value, index) => {
        subDomain[key] = value
        if (_.keys(domains).length > 1) {
            output = evaluate(_.omit(domains, key),
                expression,
                subDomain,
                output)
        } else {
            let numericalExpression = expression
            _.forEach(_.keys(subDomain), (key) => {
                numericalExpression = algebrite.run(`eval(${numericalExpression}, ${key}, ${subDomain[key]})`)
            })
            output[_.values(subDomain)] = parseFloat(numericalExpression)
        }
    })
    return output
}

/**
 * ev - Evaluate
 *
 * @param {object} data - Discrete domain sets
 * @param {string} expression - An algebrite expression
 * @return {ndarray} Expression evaluated on domains
 *
 * @example
 *
 *      e({x: [1,3,5], y:[2,4]})
 */
const ev = (domains, expression) => {
    // TODO: Validate equation
    // convert JS array to ndarray
    const nddomains = {}
    _.forEach(domains, (value, key) => {
        nddomains[key] = (typeof (value) === 'object') ? nj.float64(value) : value
    })
    // If expression is an equation, return JS object
    if (expression.includes('=')) {
        const operands = expression.replace(' ', '').split('=')

        const evaluations = evaluate(nddomains, operands[1])
        output = {}
        _.forEach(_.keys(domains), (value, index) => {
            output[value] = []
            _.forEach(_.keys(evaluations), (evaluation) => {
                output[value].push(parseFloat(evaluation.split(',')[index]))
            })
        })
        output[operands[0]] = []
        _.forEach(_.values(evaluations), (evaluation) => {
            output[operands[0]].push(evaluation)
        })
        return output
    } else {
        // determine dimension of output array
        const dimensionLengths = []
        _.forEach(nddomains, (values) => {
            dimensionLengths.push(values.size)
        })
        return nj.float64(_.values(evaluate(nddomains, expression))).reshape(...dimensionLengths)
    }
}

/**
 * s - Symbolic Computation
 *
 * @param {string} expression - An algebrite expression
 */
const s = (expression) => {
    sOut.push(algebrite.run(expression))
    const latex = algebrite.run(`printlatex(${expression})`)
    return latexHtml(latex)
}
/**
 * p - Plot
 *
 * @param {object} data - Discrete points to plot
 * @param {object} type - Title, type, etc.
 */
const p = (data, layout) => {
    plot = plt.createPlot(data, layout)
    return plot.render()
}

module.exports = { ev, evOut, nj, pRaw: p, sRaw: s, sOut }
